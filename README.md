# Stance Detection AI Service (UCLNLP)

This repository contains Stance Detecton AI service, originally developed by [UCLNLP group](http://nlp.cs.ucl.ac.uk/) for the [Fake News Challenge](http://www.fakenewschallenge.org/) in 2017, where it took the 3rd place. The original forked repository can be found on the GitHub [here](https://github.com/uclnlp/fakenewschallenge).

We have updated the algorithm to run with modern Python libraries as well as packaged it as a [SingularityNET AI Service](https://beta.singularitynet.io/servicedetails/org/nunet/service/uclnlp-service) to be executed on [NuNet private Alpha platform](https://nunet.io/nunet-alpha).

## Description

Based on pre-trained model, estimates the nature of relationship between the title of an article and the body text of an article. The estimation is returned in the form of probabilities attached to four pre-defined types of relation: unrelated, discussing, agreeing, disagreeing. Sum of all probabilities add up to one. The displayed relation has the highest probability attached by the algorithm. See the [original explanation](https://github.com/uclnlp/fakenewschallenge/blob/master/README.md) by UCLNLP group for details.

## Setup

The docker container used on the NuNet platfrom is built by CI/CD pipeline of the `master` branch of this repository and published on the NuNet package repository for each new commit. NuNet automatically picks it up and deploys on available hardware when required by a backend call of the fake-news-warning app [browser extension]().

The manual buld can be achieved in done in following way.

	# build snet_publish_service image if it doesn't exist
	docker build -t snet_publish_service https://github.com/singnet/dev-portal.git#master:/tutorials/docker

	docker build -t uclnlp_snet .
	
	# map daemon and service ports externally
	docker run -p 7020:7020 -p 7021:7021 -e nunet_adapter_address = $nunet_adapter_address -it uclnlp_snet bash

	# uclnlp server
	python3 run_uclnlp_service.py --daemon-config snetd.config.json
	python3 run_uclnlp_service.py --no-daemon

	# snet request to service (using snet or the test script)
	snet client call nunet-org-new uclnlp-service default_group stance_classify '{"headline":"news_headline","body":"news_body"}' 
	
	python3 test_uclnlp_service.py
